package br.com.cortez.desafio.model;

import java.util.List;

/**
 * Created by Pedro on 6/6/16.
 */

public class PRRequest {


    List<PullRequest> pullRequestList;

    public List<PullRequest> getPullRequestList() {
        return pullRequestList;
    }

    public void setPullRequestList(List<PullRequest> pullRequestList) {
        this.pullRequestList = pullRequestList;
    }
}
