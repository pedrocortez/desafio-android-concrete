package br.com.cortez.desafio.imageLoader.impl;

import android.content.Context;
import android.widget.ImageView;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;

import br.com.cortez.desafio.ChallengeApplication;
import br.com.cortez.desafio.imageLoader.ImageLoader;

/**
 * Created by Pedro on 6/4/16.
 */

public class PicassoImageLoader implements ImageLoader {

    public PicassoImageLoader() {

        Picasso.Builder builder = new Picasso.Builder(getContext());
        builder.downloader(new OkHttp3Downloader(getContext(), Integer.MAX_VALUE));
        Picasso built = builder.build();
        Picasso.setSingletonInstance(built);
    }

    WeakReference<Context> context;

    @Override
    public void load(final String url, final ImageView imageView) {
        Picasso.with(getContext())
                .load(url)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {}

                    @Override
                    public void onError() {
                        loadWithoutCache(url, imageView);
                    }
                });
    }

    @Override
    public void load(final String url, final ImageView imageView, final int placeHolder) {
        Picasso.with(getContext())
                .load(url)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .placeholder(placeHolder).into(imageView, new Callback() {
            @Override
            public void onSuccess() {}

            @Override
            public void onError() {
                loadWithoutCache(url, imageView, placeHolder);
            }
        });
    }

    @Override
    public void load(final String url, final ImageView imageView, final int width, final int height) {
        Picasso.with(getContext())
                .load(url)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .resize(width, height)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {}

                    @Override
                    public void onError() {
                        loadWithoutCache(url, imageView, width, height);
                    }
                });
    }

    @Override
    public void load(final String url, final ImageView imageView, final int placeholder, final int width, final int height) {
        Picasso.with(getContext())
                .load(url)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .placeholder(placeholder)
                .resize(width, height)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {}

                    @Override
                    public void onError() {
                        loadWithoutCache(url, imageView, placeholder, width, height);
                    }
                });
    }


    private void loadWithoutCache(String url, ImageView imageView) {
        Picasso.with(getContext())
                .load(url)
                .into(imageView);
    }

    private void loadWithoutCache(String url, ImageView imageView, int placeHolder) {
        Picasso.with(getContext())
                .load(url)
                .placeholder(placeHolder).into(imageView);
    }

    private void loadWithoutCache(String url, ImageView imageView, int width, int height) {
        Picasso.with(getContext())
                .load(url)
                .resize(width, height)
                .into(imageView);
    }

    private void loadWithoutCache(String url, ImageView imageView, int placeholder, int width, int height) {
        Picasso.with(getContext())
                .load(url)
                .placeholder(placeholder)
                .resize(width, height)
                .into(imageView);
    }







    private Context getContext() {
        if (context == null) {
            context = new WeakReference<Context>(ChallengeApplication.getInstance().getApplicationContext());
        }
        return context.get();
    }
}
