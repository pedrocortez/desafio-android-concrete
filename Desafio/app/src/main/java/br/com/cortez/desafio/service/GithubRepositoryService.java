package br.com.cortez.desafio.service;

/**
 * Created by Pedro on 6/5/16.
 */

public interface GithubRepositoryService {


    void  loadGithubRepositories();

    void loadMoreGithubRepositories();
}
