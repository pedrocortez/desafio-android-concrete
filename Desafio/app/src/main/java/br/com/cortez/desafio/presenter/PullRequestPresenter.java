package br.com.cortez.desafio.presenter;

import com.squareup.otto.Subscribe;

import br.com.cortez.desafio.ChallengeApplication;
import br.com.cortez.desafio.presenter.event.LoadFailed;
import br.com.cortez.desafio.presenter.event.LoadPullRequestSuccess;
import br.com.cortez.desafio.service.PullRequestService;
import br.com.cortez.desafio.view.PullRequestView;

/**
 * Created by Pedro on 6/6/16.
 */

public class PullRequestPresenter {

    private PullRequestView view;
    private PullRequestService service;

    public PullRequestPresenter(PullRequestView pullRequestView) {
        this.view = pullRequestView;
        this.service = ChallengeApplication.getInstance().providesPullRequestService();
    }

    public void start() {
        ChallengeApplication.getInstance().providesBus().register(this);
    }

    public void stop() {
        ChallengeApplication.getInstance().providesBus().unregister(this);
    }

    public void loadViews(String name, String repository) {
        view.showLoading();
        service.loadPullRequests(name, repository);
    }

    @Subscribe
    public void onLoadRepositorySuccess(LoadPullRequestSuccess loadPullRequestSuccess) {
        view.showPullRequests(loadPullRequestSuccess.getPullRequests());
    }

    @Subscribe
    public void onLoadRepositoryError(LoadFailed loadFailed) {
        view.showError();
    }


}
