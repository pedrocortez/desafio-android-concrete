
package br.com.cortez.desafio.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Repository {

    @SerializedName("name")
    private String name;
    @SerializedName("owner")
    private Owner owner;
    @SerializedName("description")
    private String description;
    @SerializedName("fork")
    private boolean fork;
    @SerializedName("stargazers_count")
    private int stargazersCount;
    @SerializedName("forks_count")
    private int forksCount;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getForksCount() {
        return forksCount;
    }

    public int getStargazersCount() {
        return stargazersCount;
    }

    public Owner getOwner() {
        return owner;
    }

}
