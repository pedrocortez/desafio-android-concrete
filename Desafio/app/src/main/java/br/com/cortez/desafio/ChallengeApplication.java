package br.com.cortez.desafio;

import android.app.Application;

import com.squareup.otto.Bus;

import br.com.cortez.desafio.imageLoader.ImageLoader;
import br.com.cortez.desafio.imageLoader.impl.PicassoImageLoader;
import br.com.cortez.desafio.repository.GithubRepository;
import br.com.cortez.desafio.repository.PullRequestRepository;
import br.com.cortez.desafio.repository.impl.GithubRepositoryImpl;
import br.com.cortez.desafio.repository.impl.PullRequestRepositoryImpl;
import br.com.cortez.desafio.service.GithubRepositoryService;
import br.com.cortez.desafio.service.PullRequestService;
import br.com.cortez.desafio.service.impl.GithubRepositoryServiceImpl;
import br.com.cortez.desafio.service.impl.PullRequestServiceImpl;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ChallengeApplication extends Application {


    private static ChallengeApplication instance;

    private ImageLoader imageLoader;

    private GithubRepository githubRepository;
    private GithubRepositoryService githubRepositoryService;

    private PullRequestRepository pullRequestRepository;
    private PullRequestService pullRequestService;

    private Retrofit retrofit;
    private Bus bus;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static ChallengeApplication getInstance() {
        return instance;
    }


    public ImageLoader providesImageLoader() {
        if (imageLoader == null) {
            imageLoader = new PicassoImageLoader();
        }
        return imageLoader;
    }

    public GithubRepository providesGithubRepository() {
        if (githubRepository == null) {
            githubRepository = new GithubRepositoryImpl();
        }
        return githubRepository;
    }

    public GithubRepositoryService providesGithubRepositoryService() {
        if (githubRepositoryService == null) {
            githubRepositoryService = new GithubRepositoryServiceImpl(providesGithubRepository());
        }
        return githubRepositoryService;
    }

    public Retrofit providesRetrofit() {


        if (retrofit == null) {

            retrofit = new Retrofit.Builder()
                    .baseUrl("https://api.github.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public Bus providesBus() {
        if (bus == null) {
            bus = new Bus();
        }
        return bus;
    }


    public PullRequestRepository providesPullRequestRepository() {
        if(pullRequestRepository == null) {
            pullRequestRepository = new PullRequestRepositoryImpl();
        }
        return pullRequestRepository;
    }

    public PullRequestService providesPullRequestService() {
        if(pullRequestService == null){
            pullRequestService = new PullRequestServiceImpl(providesPullRequestRepository());
        }
        return pullRequestService;
    }
}
