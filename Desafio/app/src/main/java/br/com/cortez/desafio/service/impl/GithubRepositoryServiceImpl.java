package br.com.cortez.desafio.service.impl;

import java.util.List;

import br.com.cortez.desafio.ChallengeApplication;
import br.com.cortez.desafio.model.Repository;
import br.com.cortez.desafio.presenter.event.LoadFailed;
import br.com.cortez.desafio.presenter.event.LoadListRepositorySuccess;
import br.com.cortez.desafio.presenter.event.LoadMoreFailed;
import br.com.cortez.desafio.repository.GithubRepository;
import br.com.cortez.desafio.repository.http.CallbackHttp;
import br.com.cortez.desafio.service.GithubRepositoryService;

/**
 * Created by Pedro on 6/5/16.
 */

public class GithubRepositoryServiceImpl implements GithubRepositoryService {

    private GithubRepository githubRepository;

    CallbackHttp<List<Repository>> listCallbackHttp;
    int page;

    public GithubRepositoryServiceImpl(GithubRepository githubRepository) {
        this.githubRepository = githubRepository;
        this.listCallbackHttp = getCallback();
    }

    private CallbackHttp<List<Repository>> getCallback() {

        return new CallbackHttp<List<Repository>>() {
            @Override
            public void success(List<Repository> param) {
                ChallengeApplication.getInstance().providesBus().post(new LoadListRepositorySuccess(param));
            }

            @Override
            public void onError(int status, String error) {
                if(page == 1) {
                    ChallengeApplication.getInstance().providesBus().post(new LoadFailed());
                } else {
                    ChallengeApplication.getInstance().providesBus().post(new LoadMoreFailed());
                }

            }
        };
    }

    @Override
    public void loadGithubRepositories() {
        page = 1;
        githubRepository.getRespositories(page, listCallbackHttp);
    }

    @Override
    public void loadMoreGithubRepositories() {
        githubRepository.getRespositories(++page, listCallbackHttp);

    }
}
