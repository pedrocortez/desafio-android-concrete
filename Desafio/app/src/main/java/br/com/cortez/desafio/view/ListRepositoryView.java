package br.com.cortez.desafio.view;

import java.util.List;

import br.com.cortez.desafio.model.Repository;

/**
 * Created by Pedro on 6/2/16.
 */

public interface ListRepositoryView {

    void showLoading();

    void hideLoading();

    void showRepositories(List<Repository> repositories);

    void showError();

    void errorLoadMoreContent();


}
