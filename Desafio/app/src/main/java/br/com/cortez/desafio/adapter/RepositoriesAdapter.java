package br.com.cortez.desafio.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import br.com.cortez.desafio.ChallengeApplication;
import br.com.cortez.desafio.R;
import br.com.cortez.desafio.imageLoader.ImageLoader;
import br.com.cortez.desafio.model.Repository;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Pedro on 6/4/16.
 */

public class RepositoriesAdapter extends RecyclerView.Adapter<RepositoriesAdapter.ViewHolder> {

    private List<Repository> repositoryList;
    private ImageLoader imageLoader;
    private WeakReference<Context> contextWeakReference;
    private RepositoryClickListener repositoryClickListener;

    public RepositoriesAdapter(RepositoryClickListener repositoryClickListener) {
        this.repositoryClickListener = repositoryClickListener;
        this.repositoryList = new ArrayList<>();
        this.imageLoader = ChallengeApplication.getInstance().providesImageLoader();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_repositories_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Repository repository = repositoryList.get(position);

        final String username = repository.getOwner().getLogin();
        final String repositoryName = repository.getName();

        String url = repository.getOwner().getAvatarUrl();
        int size = (int) getContext().getResources().getDimension(R.dimen.profile_size);


        holder.nameRepository.setText(repositoryName);
        holder.description.setText(repository.getDescription());
        holder.forks.setText(Integer.toString(repository.getForksCount()));
        holder.stars.setText(Integer.toString(repository.getStargazersCount()));
        holder.username.setText(username);

        imageLoader.load(url, holder.profilePic, size, size);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repositoryClickListener.onItemClick(username, repositoryName);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (repositoryList == null) {
            return 0;
        }
        return repositoryList.size();
    }


    public void addItens(List<Repository> repositoryList) {
        this.repositoryList.addAll(repositoryList);
    }

    private Context getContext() {
        if (contextWeakReference == null) {
            contextWeakReference = new WeakReference<Context>(ChallengeApplication.getInstance());
        }
        return contextWeakReference.get();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.listRepositories_nameRepo)
        TextView nameRepository;
        @Bind(R.id.listRepositories_description)
        TextView description;
        @Bind(R.id.listRepositories_forks)
        TextView forks;
        @Bind(R.id.listRepositories_stars)
        TextView stars;
        @Bind(R.id.listRepositories_username)
        TextView username;
        @Bind(R.id.listRepositories_profile)
        ImageView profilePic;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface RepositoryClickListener {

        void onItemClick(String username, String repositoryName);
    }
}
