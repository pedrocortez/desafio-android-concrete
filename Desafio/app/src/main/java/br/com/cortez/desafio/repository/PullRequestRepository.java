package br.com.cortez.desafio.repository;

import java.util.List;

import br.com.cortez.desafio.model.PullRequest;
import br.com.cortez.desafio.repository.http.CallbackHttp;

/**
 * Created by Pedro on 6/6/16.
 */

public interface PullRequestRepository {

    void getPullRequests(String name, String repository,  CallbackHttp<List<PullRequest>> callbackHttp);
}
