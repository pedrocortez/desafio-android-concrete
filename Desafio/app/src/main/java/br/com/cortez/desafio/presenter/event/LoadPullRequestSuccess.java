package br.com.cortez.desafio.presenter.event;

import java.util.List;

import br.com.cortez.desafio.model.PullRequest;

/**
 * Created by Pedro on 6/6/16.
 */

public class LoadPullRequestSuccess {

    private List<PullRequest> pullRequests;

    public LoadPullRequestSuccess(List<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;
    }

    public List<PullRequest> getPullRequests() {
        return pullRequests;
    }
}
