package br.com.cortez.desafio.repository;

import java.util.List;

import br.com.cortez.desafio.model.Repository;
import br.com.cortez.desafio.repository.http.CallbackHttp;

public interface GithubRepository {


    void getRespositories(int page, CallbackHttp<List<Repository>> callbackHttp);



}
