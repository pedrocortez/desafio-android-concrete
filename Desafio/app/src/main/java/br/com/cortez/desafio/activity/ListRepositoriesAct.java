package br.com.cortez.desafio.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import br.com.cortez.desafio.R;
import br.com.cortez.desafio.adapter.RepositoriesAdapter;
import br.com.cortez.desafio.decoration.VerticalSelectionDividerItemDecoration;
import br.com.cortez.desafio.model.Repository;
import br.com.cortez.desafio.presenter.ListRepositoryPresenter;
import br.com.cortez.desafio.view.ListRepositoryView;
import br.com.cortez.desafio.viewListener.EndlessRecyclerOnScrollListener;
import butterknife.Bind;
import butterknife.ButterKnife;

public class ListRepositoriesAct extends AppCompatActivity implements ListRepositoryView {


    @Bind(R.id.listRepositories_recycler)
    RecyclerView repositories;

    @Bind(R.id.listRepositories_pullrefresh)
    SwipeRefreshLayout swipeContainer;

    @Bind(R.id.layoutLoading)
    View loading;

    @Bind(R.id.error_layout)
    View error;

    ListRepositoryPresenter presenter;

    RepositoriesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_repositories);
        ButterKnife.bind(this);

        adapter = new RepositoriesAdapter(new RepositoriesAdapter.RepositoryClickListener() {
            @Override
            public void onItemClick(String username, String repositoryName) {
                PullRequestAct.start(ListRepositoriesAct.this, username, repositoryName);
            }
        });



        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        VerticalSelectionDividerItemDecoration verticalSelectionDividerItemDecoration =
                new VerticalSelectionDividerItemDecoration(R.dimen.space_recycler, 1);

        repositories.addItemDecoration(verticalSelectionDividerItemDecoration);
        repositories.setAdapter(adapter);
        repositories.setLayoutManager(linearLayoutManager);



        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.loadViews();
            }
        });

        repositories.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                presenter.loadMoreRepositories();
            }
        });


        showLoading();

        presenter = new ListRepositoryPresenter(this);
        presenter.loadViews();

    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.start();
    }

    @Override
    protected void onStop() {
        presenter.stop();
        super.onStop();
    }

    @Override
    public void showLoading() {
        loading.setVisibility(View.VISIBLE);
        repositories.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        loading.setVisibility(View.GONE);
        error.setVisibility(View.GONE);
        repositories.setVisibility(View.VISIBLE);
    }

    @Override
    public void showRepositories(List<Repository> list) {

        hideLoading();

        adapter.addItens(list);
        adapter.notifyDataSetChanged();

        swipeContainer.setRefreshing(false);

    }

    @Override
    public void showError() {
        hideLoading();
        swipeContainer.setRefreshing(false);
        error.setVisibility(View.VISIBLE);
    }

    @Override
    public void errorLoadMoreContent() {
        swipeContainer.setRefreshing(false);
        Toast.makeText(this, getString(R.string.listrepositories_error_content), Toast.LENGTH_LONG).show();
    }
}
