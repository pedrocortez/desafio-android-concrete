package br.com.cortez.desafio.presenter;

import com.squareup.otto.Subscribe;

import br.com.cortez.desafio.ChallengeApplication;
import br.com.cortez.desafio.presenter.event.LoadFailed;
import br.com.cortez.desafio.presenter.event.LoadListRepositorySuccess;
import br.com.cortez.desafio.presenter.event.LoadMoreFailed;
import br.com.cortez.desafio.service.GithubRepositoryService;
import br.com.cortez.desafio.view.ListRepositoryView;

public class ListRepositoryPresenter {

    private ListRepositoryView view;
    private GithubRepositoryService service;

    public ListRepositoryPresenter(ListRepositoryView view) {
        this.view = view;
        this.service = ChallengeApplication.getInstance().providesGithubRepositoryService();

    }

    public void start() {
        ChallengeApplication.getInstance().providesBus().register(this);
    }

    public void stop() {
        ChallengeApplication.getInstance().providesBus().unregister(this);
    }

    public void loadViews() {
        service.loadGithubRepositories();
    }

    @Subscribe
    public void onLoadRepositorySuccess(LoadListRepositorySuccess loadListRepositorySuccess) {
        view.showRepositories(loadListRepositorySuccess.getRepositories());
    }

    @Subscribe
    public void onLoadRepositoryError(LoadFailed loadFailed) {
        view.showError();
    }

    @Subscribe
    public void onLoadRepositoryError(LoadMoreFailed loadMoreRepositoriesFailed) {
        view.errorLoadMoreContent();
    }

    public void loadMoreRepositories() {
        service.loadMoreGithubRepositories();
    }

}
