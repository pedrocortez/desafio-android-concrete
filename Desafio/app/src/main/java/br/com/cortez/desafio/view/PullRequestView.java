package br.com.cortez.desafio.view;

import java.util.List;

import br.com.cortez.desafio.model.PullRequest;

/**
 * Created by Pedro on 6/6/16.
 */

public interface PullRequestView {

    void showLoading();

    void hideLoading();

    void showPullRequests(List<PullRequest> pullRequests);

    void showError();

}
