package br.com.cortez.desafio.presenter.event;

import java.util.List;

import br.com.cortez.desafio.model.Repository;

/**
 * Created by Pedro on 6/5/16.
 */

public class LoadListRepositorySuccess {

    private List<Repository> repositories;

    public LoadListRepositorySuccess(List<Repository> repositories) {
        this.repositories = repositories;
    }

    public List<Repository> getRepositories() {
        return repositories;
    }
}
