package br.com.cortez.desafio.repository.http;

/**
 * Created by Pedro on 6/5/16.
 */

public interface CallbackHttp<T> {

    void success(T param);

    void onError(int status, String error);
}
