package br.com.cortez.desafio.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import br.com.cortez.desafio.ChallengeApplication;
import br.com.cortez.desafio.R;
import br.com.cortez.desafio.adapter.PullRequestAdapter;
import br.com.cortez.desafio.decoration.VerticalSelectionDividerItemDecoration;
import br.com.cortez.desafio.model.PullRequest;
import br.com.cortez.desafio.presenter.PullRequestPresenter;
import br.com.cortez.desafio.repository.http.CallbackHttp;
import br.com.cortez.desafio.view.PullRequestView;
import butterknife.Bind;
import butterknife.ButterKnife;

public class PullRequestAct extends AppCompatActivity implements PullRequestView {

    @Bind(R.id.listRepositories_recycler)
    RecyclerView recyclerView;

    @Bind(R.id.listRepositories_pullrefresh)
    SwipeRefreshLayout swipeContainer;

    @Bind(R.id.layoutLoading)
    View loading;

    @Bind(R.id.error_layout)
    View error;

    private String username;

    private String repository;

    private PullRequestAdapter adapter;

    private PullRequestPresenter presenter;

    public static void start(Activity activity, String username, String repositoryName) {
        Intent i = new Intent(activity, PullRequestAct.class);
        Bundle bundle = new Bundle();
        bundle.putString("username", username);
        bundle.putString("repositoryName", repositoryName);
        i.putExtras(bundle);
        activity.startActivity(i);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_repositories);
        ButterKnife.bind(this);

        consumeIntent();

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(repository);

        adapter = new PullRequestAdapter(new PullRequestAdapter.PullRequestClickListener() {
            @Override
            public void onItemClick(String url) {
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(myIntent);
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        VerticalSelectionDividerItemDecoration verticalSelectionDividerItemDecoration =
                new VerticalSelectionDividerItemDecoration(R.dimen.space_recycler, 1);

        recyclerView.addItemDecoration(verticalSelectionDividerItemDecoration);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(linearLayoutManager);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.loadViews(username, repository);
            }
        });

        presenter = new PullRequestPresenter(this);
        presenter.loadViews(username, repository);

    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.start();
    }

    @Override
    protected void onStop() {
        presenter.stop();
        super.onStop();
    }

    @Override
    public void showLoading() {
        loading.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        loading.setVisibility(View.GONE);
        error.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showPullRequests(List<PullRequest> pullRequests) {
        hideLoading();

        adapter.addItens(pullRequests);
        adapter.notifyDataSetChanged();

        swipeContainer.setRefreshing(false);

    }

    @Override
    public void showError() {
        hideLoading();
        swipeContainer.setRefreshing(false);
        error.setVisibility(View.VISIBLE);
    }

    private void consumeIntent() {
        Bundle extras = getIntent().getExtras();
        username   = extras.getString("username");
        repository = extras.getString("repositoryName");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}