package br.com.cortez.desafio.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import br.com.cortez.desafio.ChallengeApplication;


public class NetworkUtil {

    public static boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) ChallengeApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
