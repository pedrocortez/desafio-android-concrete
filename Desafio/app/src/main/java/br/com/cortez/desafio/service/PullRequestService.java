package br.com.cortez.desafio.service;

/**
 * Created by Pedro on 6/6/16.
 */

public interface PullRequestService {

    void loadPullRequests(String name, String repository);

}
