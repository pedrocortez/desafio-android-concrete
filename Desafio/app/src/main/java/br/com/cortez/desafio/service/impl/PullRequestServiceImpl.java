package br.com.cortez.desafio.service.impl;

import java.util.List;

import br.com.cortez.desafio.ChallengeApplication;
import br.com.cortez.desafio.model.PullRequest;
import br.com.cortez.desafio.presenter.event.LoadFailed;
import br.com.cortez.desafio.presenter.event.LoadMoreFailed;
import br.com.cortez.desafio.presenter.event.LoadPullRequestSuccess;
import br.com.cortez.desafio.repository.PullRequestRepository;
import br.com.cortez.desafio.repository.http.CallbackHttp;
import br.com.cortez.desafio.service.PullRequestService;

/**
 * Created by Pedro on 6/6/16.
 */

public class PullRequestServiceImpl implements PullRequestService {


    private PullRequestRepository pullRequestRepository;

    CallbackHttp<List<PullRequest>> listCallbackHttp;

    public PullRequestServiceImpl(PullRequestRepository pullRequestRepository) {
        this.pullRequestRepository = pullRequestRepository;
        this.listCallbackHttp = getCallback();
    }

    @Override
    public void loadPullRequests(String name, String repository) {
        pullRequestRepository.getPullRequests(name, repository, listCallbackHttp);

    }

    private CallbackHttp<List<PullRequest>> getCallback() {
        return new CallbackHttp<List<PullRequest>>() {
            @Override
            public void success(List<PullRequest> param) {
                ChallengeApplication.getInstance().providesBus().post(new LoadPullRequestSuccess(param));
            }

            @Override
            public void onError(int status, String error) {
                ChallengeApplication.getInstance().providesBus().post(new LoadFailed());
            }
        };
    }
}
