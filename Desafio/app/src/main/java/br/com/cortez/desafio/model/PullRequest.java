package br.com.cortez.desafio.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Pedro on 6/6/16.
 */

//Cada item da lista deve exibir Nome / Foto do autor do PR, Título do PR, Data do PR e Body do PR

public class PullRequest {

    @SerializedName(value = "html_url")
    private String url;

    @SerializedName(value = "title")
    private String title;

    @SerializedName(value = "user")
    private Owner user;

    @SerializedName(value = "body")
    private String description;

    @SerializedName(value = "created_at")
    private String create;


    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }

    public Owner getUser() {
        return user;
    }

    public String getDescription() {
        return description;
    }

    public String getCreate() {
        return create;
    }
}
